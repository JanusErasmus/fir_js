import pyduktape
import json
import matplotlib.pyplot as plt
import numpy as np
import lowpass_35 as response

sample_rate = 600 #0.000033333
sample_time = 86400 * 4#0.006
f = 1/86400 #1000
f_noise = 1/2000 #6000
t = np.arange(0.0, sample_time, sample_rate)
# v_sin = 1 + np.sin((2 * f * np.pi * t) + np.pi) #+ 0.5 * np.sin(2 * f_noise * np.pi * t)
v_sin = np.zeros(len(t))
for i in range (40,len(t)):#range(int(len(t)/2), len(t)):
    v_sin[i] = 1

v_in = v_sin + np.random.normal(0,0.1,len(t))

print("Evluate a Javascript in python")

print(f"N   : {len(t)}")
print(f"Order: {len(response.h)}")
print(f"Time: {sample_time}")
print(f"Fin : {f}")
print(f"Fn  : {f_noise}")
print(f"Ts  : {sample_rate}")
print(f"Fs  : {1/sample_rate}")
context = pyduktape.DuktapeContext()
obj = context.eval_js_file('fir.js')

res = obj.new(json.dumps(response.h))
# res.print_h()

sim_in = list(v_in)
v_out = list(res.filter(json.dumps(sim_in)))


fig, ax = plt.subplots(3, 1)
ax[0].plot(t, v_sin, marker=".")
ax[1].plot(t, v_in, marker=".")
ax[2].plot(t, v_out, marker=".")
plt.show()