# Implement a FIR filter in Javascript

 - [pyduktape](https://pypi.org/project/pyduktape/) was used to test the filter
 - [matplotlib](https://matplotlib.org/) was used to plot results of the filter
 - Run `python3 main.py` to plot a response