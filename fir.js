function FIR(h) {
    //remove this when everything is Javascript
    this.h = JSON.parse(h);
    this.order = this.h.length;
}

FIR.prototype.print_h = function () {
    print('Order : ' + this.order);
    print('H     : ' + JSON.stringify(this.h));
};

FIR.prototype.get_h = function () {
    return this.h
}

FIR.prototype.filter = function (v_in) {
    v_array = JSON.parse(v_in);
    var len = v_array.length

    //Initialize empty output array
    v_out = new Array(len)

    for (var k = 0; k < len; k++){
        v_out[k] = 0;
        for (var h = this.order - 1; h >= 0; h--){
            var x = 0;
            var idx = k - h;
            if(idx >= 0){
                x = v_array[idx];
            }

            v_out[k] +=  this.h[h] * x;
        }
    }
    return v_out
}

FIR
